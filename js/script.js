$(document).ready(function () {
  var map = L.map('map').setView([0, 0], 13); // Inicialmente, el centro del mapa se establece en coordenadas (0, 0)
  var userMarker; // Variable para almacenar el marcador del usuario
  var routingControl; // Variable para almacenar el control de enrutamiento

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
  }).addTo(map);

  // Obtener la ubicación en tiempo real del usuario al cargar la página
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var userCoords = [position.coords.latitude, position.coords.longitude];
      map.setView(userCoords, 13); // Centrar el mapa en la ubicación del usuario
      userMarker = L.marker(userCoords).addTo(map); // Agregar un marcador en la ubicación del usuario
    }, function (error) {
      alert('Error al obtener la ubicación: ' + error.message);
    });
  } else {
    alert('La geolocalización no es compatible con este navegador.');
  }

  // Manejar el click en los botones para mostrar las rutas
  $('.route-button').click(function () {
    var destination;
    var color = $(this).css('background-color');
    var info; // Variable para almacenar la información del pueblo
    var photoUrl; // Variable para almacenar la URL de la foto

    switch ($(this).attr('id')) {
      case 'cosala':
        destination = [24.4, -106.6833];
        info = "Cosalá se encuentra ubicado en la Sierra Madre Occidental, a 160 kilómetros de Culiacán. Es un reflejo del México antiguo con atributos simbólicos, leyendas e historias. Este pueblo conserva su traza urbana, la forma, ancho de sus calles y banquetas, así como la altura de estas últimas.";
        photoUrl = "/img/cosala.jpg"; // URL de la foto de Cosalá
        break;
      case 'mocorito':
        destination = [25.4833, -107.9167];
        info = "Este Pueblo Mágico es particularmente sabroso pues es cuna del Chilorio, un guiso de carne de cerdo deshebrada con chile pasilla y secretos locales que algunos consideran patrimonio cultural. También porque es un destino donde vives la esencia del norte por medio de la fiesta, la música de banda o a través de actividades en la naturaleza como ir de pesca o intentar jugar el antiguo deporte de Ulama que aquí sigue siendo una práctica cotidiana.";
        photoUrl = "/img/mocorito.jpg"; // URL de la foto de Mocorito
        break;
      case 'el_fuerte':
        destination = [26.4333, -108.6167];
        info = "El Municipio del El Fuerte se localiza en la parte noroeste del estado de Sinaloa; posee grandes atractivos naturales, históricos, arquitectónicos, culturales y arraigadas tradiciones indígenas (Yoremes). Se ubica dentro del Circuito Ecoturístico Mar de Cortés – Barrancas del Cobre. El mayor de sus atractivos turísticos es su cabecera municipal, la ciudad colonial de El Fuerte, fundada en 1564 como la Villa de San Juan Bautista de Carapoa, por el capitán español Francisco de Ibarra conocido como El Fénix de los Conquistadores";
        photoUrl = "/img/elfuerte.jpg"; // URL de la foto de El Fuerte
        break;
      case 'el_rosario':
        destination = [22.75, -105.3833];
        info = "El Rosario es considerado Pueblo Mágico por su riqueza histórica acumulada por más de tres siglos, es un poblado de tradición minera fundado en 1655 que alcanzó su mayor auge político y económico en el siglo XVIII. A pocos kilómetros de distancia, en Chametla se puede visitar un importante centro ceremonial indígena.";
        photoUrl = "/img/elrosario.jpg"; // URL de la foto de El Rosario
        break;
    }

    // Mostrar información del pueblo y la foto
    $('#info').html(info);
    $('#photo').attr('src', photoUrl);

    // Mover el mapa hacia la izquierda
    $('#map').css('float', 'left');

    showRouteToDestination(destination, color); // Pasar el color como argumento
  });

  // Manejar el click en el botón de limpiar rutas
  $('#clear').click(function () {
    // Limpiar información del pueblo y la foto
    $('#info').html('');
    $('#photo').attr('src', '');

    // Mover el mapa de vuelta al centro
    $('#map').css('float', 'none');

    if (routingControl) {
      map.removeControl(routingControl); // Eliminar el control de enrutamiento del mapa
      routingControl = null;
    }
  });

  // Función para mostrar la ruta desde la ubicación actual del usuario hasta el destino seleccionado
  function showRouteToDestination(destinationCoords, color) {
    if (!userMarker) {
      alert('No se puede encontrar la ubicación del usuario.');
      return;
    }

    var userCoords = userMarker.getLatLng(); // Obtener las coordenadas del marcador del usuario
    var waypoints = [
      L.latLng(userCoords.lat, userCoords.lng), // Ubicación actual del usuario
      L.latLng(destinationCoords[0], destinationCoords[1]) // Coordenadas del destino seleccionado
    ];

    // Limpiar rutas anteriores si ya existen
    if (routingControl) {
      map.removeControl(routingControl); // Eliminar el control de enrutamiento del mapa
      routingControl = null;
    }

    // Crear el control de enrutamiento con el color del botón actual
    routingControl = L.Routing.control({
      waypoints: waypoints,
      routeWhileDragging: true,
      lineOptions: {
        styles: [{ color: color, weight: 8 }] // Usar el color pasado como argumento
      }
    }).addTo(map);
  }
});
